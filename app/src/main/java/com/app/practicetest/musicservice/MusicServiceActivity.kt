package com.app.practicetest.musicservice

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import com.app.practicetest.R
import com.app.practicetest.databinding.ActivityMusicServiceBinding

class MusicServiceActivity : AppCompatActivity() {

    lateinit var service : MusicService
    var isBound : Boolean = false
    lateinit var binding : ActivityMusicServiceBinding

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, iBinder : IBinder?) {
            val binding = iBinder as MusicService.MyIBinder

            service = binding.getService()
            isBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMusicServiceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        runService()

        binding.btnPlayPause.setOnClickListener {

            if (service.isPlay()) {
                service.musicPause()
                binding.btnPlayPause.text = "Play"
                binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_play_arrow,0)
            } else {
                service.musicPlay()
                binding.btnPlayPause.text = " Pause"
                binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_pause,0)
            }
        }

        binding.btnStop.setOnClickListener {
            service.musicStop()
            binding.btnPlayPause.text = "Play"
            binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_play_arrow,0)
        }

    }

    private fun runService() {
        val intent = Intent(this, MusicService::class.java)
        bindService(intent, connection, BIND_AUTO_CREATE)
    }

    private fun stopService() {
        val intent = Intent(this, MusicService::class.java)
        stopService(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
//        stopService()
    }
}