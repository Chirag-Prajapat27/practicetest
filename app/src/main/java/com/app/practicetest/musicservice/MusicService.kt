package com.app.practicetest.musicservice

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import androidx.core.app.NotificationCompat
import com.app.practicetest.R

class MusicService : Service() {

    private lateinit var mediaPlayer: MediaPlayer
    private val binder = MyIBinder()

    override fun onBind(intent: Intent): IBinder {
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)
        mediaPlayer.isLooping = true
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val channelID = getString(R.string.app_name)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getSystemService(NotificationManager::class.java)
                .createNotificationChannel(
                    NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_LOW)
                )
        }

        val notification =
            NotificationCompat.Builder(this, channelID)
            .setContentText("MusicPlayer")
            .setContentTitle(if (this.isPlay()) "Play" else "Pause")
            .setSmallIcon(if (this.isPlay()) R.drawable.ic_pause else R.drawable.ic_play_arrow)
            .build()


        //BackGround to Foreground Service
        startForeground(102, notification)

        return START_STICKY
    }

    inner class MyIBinder : Binder() {
        fun getService(): MusicService {
            return this@MusicService
        }
    }

    fun musicPlay() {
        mediaPlayer.start()
    }

    fun musicPause() {
        mediaPlayer.pause()
    }

    fun isPlay(): Boolean {
        return mediaPlayer.isPlaying
    }

    fun musicStop() {
        mediaPlayer.reset()
    }
}
