package com.app.practicetest.connection

sealed class NetworkStatus {

    object Available : NetworkStatus()
    object Unavailable : NetworkStatus()

}