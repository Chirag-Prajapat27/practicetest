package com.app.practicetest.utils

const val CHANNEL_ID = "CHANNEL_ID"


class Constants {
    interface ACTION {
        companion object {
            const val MAIN_ACTION = "action.main"
            const val PREV_ACTION = "action.prev"
            const val PLAY_ACTION = "action.play"
            const val PAUSE_ACTION = "action.pause"
            const val NEXT_ACTION = "action.next"
            const val CLOSE_ACTION = "action.close"
            const val STARTFOREGROUND_ACTION = "action.startforeground"
            const val STOPFOREGROUND_ACTION = "action.stopforeground"
        }
    }

    interface NOTIFICATION_ID {
        companion object {
            const val FOREGROUND_SERVICE = 101
        }
    }
}