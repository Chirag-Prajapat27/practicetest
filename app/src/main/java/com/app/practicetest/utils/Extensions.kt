package com.app.practicetest.utils

import android.app.Activity
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import com.app.practicetest.R

fun View.linearGradientBackground(dominantColor : Int): GradientDrawable {

    return GradientDrawable().apply {
        colors = intArrayOf(
            dominantColor,
            Color.parseColor("#2E2929"),
            Color.parseColor("#171616")
        )
        gradientType = GradientDrawable.LINEAR_GRADIENT
        orientation = GradientDrawable.Orientation.TOP_BOTTOM
    }}

fun Context.createNotification(activity: Activity): Notification {
    val contentIntent = Intent(this, activity::class.java)
    val contentPendingIntent = PendingIntent.getActivity(this, 0, contentIntent, 0)

    return NotificationCompat.Builder(this, CHANNEL_ID)
        .setSmallIcon(R.drawable.bg_solid_theme_radius_20)
        .setColor(ResourcesCompat.getColor(this.resources, R.color.purple_200, null))
        .setContentTitle(this.getString(R.string.notification_title))
        .setAutoCancel(true)
        .setContentIntent(contentPendingIntent)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .build()
}