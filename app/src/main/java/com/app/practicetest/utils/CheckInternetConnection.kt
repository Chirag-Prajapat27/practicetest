package com.app.practicetest.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import com.app.practicetest.connection.NetworkStatus

//class CheckInternetConnection(private val connectivityManager: ConnectivityManager) : LiveData<NetworkStatus>() {
class CheckInternetConnection(context: Context) : LiveData<NetworkStatus>() {

    private var connectivityManager : ConnectivityManager

    init {
           connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }


//    constructor(appContext: Application) : this(
//        appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//    )


    private val networkCallback = object : ConnectivityManager.NetworkCallback() {

        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            Log.d("ContentValues","On Available: Network is available")
            postValue(NetworkStatus.Available)
//            postValue(true)
        }

        override fun onUnavailable() {
            super.onUnavailable()
            Log.d("ContentValues","On Unavailable: Network is not available")
            postValue(NetworkStatus.Unavailable)
//            postValue(false)
        }


        @RequiresApi(Build.VERSION_CODES.M)
        override fun onCapabilitiesChanged(
            network: Network,
            networkCapabilities: NetworkCapabilities
        ) {
            super.onCapabilitiesChanged(network, networkCapabilities)
            val isInternet =networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            Log.d("ContentValues","Network Capabilities: $network $networkCapabilities")

            val isValidated =networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)

            if(isValidated)
                Log.d("ContentValues","hasCapability: $network $networkCapabilities")
            else
                Log.d("ContentValues","Network has not Capability: $network $networkCapabilities")
//            postValue(isInternet && isValidated)

            if (isInternet && isValidated)
                postValue(NetworkStatus.Available)
            else
                postValue(NetworkStatus.Unavailable)

        }

        override fun onLost(network: Network) {
            super.onLost(network)
            Log.d("ContentValues","On Lost: Network is Lost")
            postValue(NetworkStatus.Unavailable)
        }
    }

    override fun onActive() {
        super.onActive()
        val builder = NetworkRequest.Builder()
        connectivityManager.registerNetworkCallback(builder.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build(), networkCallback)
    }

    override fun onInactive() {
        super.onInactive()
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }

}