package com.app.practicetest

import android.app.Application
import androidx.lifecycle.LiveData
import com.app.practicetest.connection.NetworkStatus
import com.app.practicetest.connection.NetworkStatusHelper
import com.app.practicetest.utils.CheckInternetConnection
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

//private lateinit var called : LiveData<Boolean>
private lateinit var called : LiveData<NetworkStatus>
private var isConnect : Boolean = false

    companion object {

        lateinit var appInstance : App

        @Synchronized
        fun getInstance():App {
            return appInstance
        }

    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        called = NetworkStatusHelper(this)
        observeConnectionStatus()
    }

    fun isConnectionAvailable() : Boolean {
        return isConnect
    }

    private fun observeConnectionStatus() {
        called.observeForever {
//            CheckInternetConnection(this).observeForever {
                isConnect = when (it) {
                    NetworkStatus.Available -> true
                    NetworkStatus.Unavailable -> false
                }
//            }
        }
    }
}