package com.app.practicetest.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.practicetest.App
import com.app.practicetest.R
import com.app.practicetest.connection.NetworkStatus
import com.app.practicetest.connection.NetworkStatusHelper
import com.app.practicetest.databinding.ActivityRetrofitMainBinding
import com.app.practicetest.utils.CheckInternetConnection

class RetrofitMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityRetrofitMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_retrofit_main)

        binding.btnCheckConnection.setOnClickListener {
            binding.tvConnectionText.text = if (App.getInstance().isConnectionAvailable()) {
                "Network connection Established"
            } else { "No Internet" }

        }


        CheckInternetConnection(this).observe(this) {
            binding.tvConnectionText.text = when (it) {
                NetworkStatus.Available -> "Network connection Established"
                NetworkStatus.Unavailable -> "No Internet"
            }
        }

//        NetworkStatusHelper(this@RetrofitMainActivity).observe(this) {
//            binding.tvConnectionText.text = when (it) {
//                NetworkStatus.Available -> "Network connection Established"
//                NetworkStatus.Unavailable -> "No Internet"
//            }
//        }

    }
}