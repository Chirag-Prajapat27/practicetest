package com.app.practicetest.retrofit

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class RetrofitViewModel @Inject constructor(repository: Repository) : ViewModel() {

}
