package com.app.practicetest.retrofit

import com.app.practicetest.api.ApiServices
import com.app.practicetest.pagination.QuotesModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class Repository @Inject constructor(private val services: ApiServices){

    suspend fun getAllData() : Flow<QuotesModel> = flow {


        val response = services.getAllData()
        emit(response)
    }

}