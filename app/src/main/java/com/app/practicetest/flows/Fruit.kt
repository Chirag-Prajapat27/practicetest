package com.app.practicetest.flows

data class Fruit( val id: Int, val name: String, val price: Int, val qty: Int) {

    fun getTotal(): Int {
        return price * qty
    }
}

data class Category(var name: String, val productList: List<Fruit>) {
    fun getSortBy() : List<Fruit> {
        return productList.sortedBy { it.price }
    }

    fun getSortByDescending() : List<Fruit> {
        return productList.sortedByDescending { it.price }
    }
}
