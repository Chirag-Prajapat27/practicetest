package com.app.practicetest.flows

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.app.practicetest.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.reduce
import kotlinx.coroutines.launch

class FlowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flow)

        val countList = arrayOf(1,2,3,4,5,6,7,8,9,10).asFlow()
        val apple = Fruit(1,"Apple", 100, 5)
        val orange = Fruit(2,"Orange", 150, 10)
        val banana = Fruit(3,"Banana", 50, 20)
        val cherry = Fruit(4,"Cherry", 200, 8)
        val mango = Fruit(5,"Mango", 300, 12)

        val fruitList = arrayListOf(apple,orange,/*banana,cherry,mango*/).asFlow()

        CoroutineScope(Dispatchers.IO).launch {
            val delivery = fruitList.reduce { accumulator, value ->
                Fruit(101,"totalPrice",accumulator.getTotal() + value.getTotal(), accumulator.qty+value.qty)
            }
            Log.d("MyLog", "Total $delivery")
            fruitList.collect {

            }
        }

    }
}
