package com.app.practicetest.producttest

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.practicetest.api.ApiServices
import com.app.practicetest.pagination.ProductData
import kotlinx.coroutines.delay
import javax.inject.Inject

private const val LOAD_DELAY_MILLIS = 3_000L

class ProductPagingDataSourceHilt @Inject constructor(private val apiServices: ApiServices): PagingSource<Int, ProductData>() {

    override fun getRefreshKey(state: PagingState<Int, ProductData>): Int? {

        return state.anchorPosition?.let { anchorPosition->

            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductData> {

        val currentKey =params.key?: 1

        val response = apiServices.getAllProduct(currentKey)

//        val responseData = mutableListOf<ProductData>()
        val data = response.product_data
//        responseData.addAll(data)

        val prevKey = if(currentKey==1) null else currentKey-1

        if (currentKey  != 1) delay(LOAD_DELAY_MILLIS)
        return LoadResult.Page(data,prevKey,currentKey.plus(1))

    }
}