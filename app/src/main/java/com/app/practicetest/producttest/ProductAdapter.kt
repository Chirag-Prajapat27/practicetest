package com.app.practicetest.producttest

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.practicetest.databinding.ItemProductBinding
import com.app.practicetest.databinding.ItemQuotesBinding
import com.app.practicetest.pagination.ProductData
import javax.inject.Inject

class ProductAdapter @Inject constructor() : PagingDataAdapter<ProductData, ProductAdapter.MyViewHolder>(DiffObj) {

    class MyViewHolder(binding: ItemProductBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.productList = getItem(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    object DiffObj : DiffUtil.ItemCallback<ProductData>() {
        override fun areItemsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem == newItem
        }
    }

}
