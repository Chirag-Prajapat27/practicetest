package com.app.practicetest.pagination

data class Product(
    val current_page: Int?,
    val first_page_url: String?,
    val last_page: Int?,
    val last_page_url: String?,
    val next_page_url: Any?,
    val prev_page_url: Any?,
    val product_data: List<ProductData>)

data class ProductData(
    val cat_id: Int?,
    val category_name: String?,
    val id: Int?,
    val offer_discount: String?,
    val offer_price: String?,
    val photo: List<String?>,
    val price: String?,
    val slug: String?,
    val status: String?,
    val title: String?,

//    val date: String,
//    val description: String,
//    val reviews: Reviews,
)

//data class Reviews(
//    val avg_rating: Int,
//    val reviews_data: List<ReviewsData>,
//    val total_review: Int
//)
//
//data class ReviewsData(
//    val rate: Int,
//    val review: String,
//    val user_name: String
//)
