package com.app.practicetest.producttest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.app.practicetest.R
import com.app.practicetest.databinding.ActivityPaginationBinding
import com.app.practicetest.pagination.PagingViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class ProductPaginationActivity : AppCompatActivity() {

    private val pagingViewModel: PagingViewModel by viewModels()

    @Inject
    lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityPaginationBinding = DataBindingUtil.setContentView(this, R.layout.activity_pagination)

        binding.rvQuotes.adapter = adapter

        lifecycleScope.launchWhenStarted {
            adapter.loadStateFlow.collect {
                binding.prependProgress.isVisible = it.source.prepend is LoadState.Loading
                binding.appendProgress.isVisible = it.source.append is LoadState.Loading
            }
        }

        lifecycleScope.launchWhenStarted {
                pagingViewModel.productLIst.collectLatest {
                    adapter.submitData(it)
                }
        }

//        lifecycleScope.launchWhenStarted {
//            pagingViewModel.quoteList.collect {
//                Log.d("Data", "Hilt Data paging $it")
//                adapter.submitData(it)
//            }
//        }

    }
}