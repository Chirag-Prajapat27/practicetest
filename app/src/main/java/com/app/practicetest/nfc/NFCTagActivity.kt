package com.app.practicetest.nfc

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.app.practicetest.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NFCTagActivity : AppCompatActivity() {

    private lateinit var nfcAdapter: NfcAdapter
    private lateinit var pendingIntent: PendingIntent
    private lateinit var intentFiltersArray: Array<IntentFilter>
    private lateinit var techListsArray: Array<Array<String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfctag)

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), PendingIntent.FLAG_IMMUTABLE)

        val ndefIntentFilter = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        try {
            ndefIntentFilter.addDataType("*/*") // Handle all MIME types
        } catch (e: IntentFilter.MalformedMimeTypeException) {
            throw RuntimeException("Failed to add data type", e)
        }
        intentFiltersArray = arrayOf(ndefIntentFilter)

        techListsArray = arrayOf(arrayOf(Ndef::class.java.name), arrayOf(NdefFormatable::class.java.name))
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFiltersArray, techListsArray)
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let { handleNfcIntent(it) }

      /*  if (NfcAdapter.ACTION_NDEF_DISCOVERED == intent!!.action) {
            val rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            if (rawMessages != null) {
                val messages = arrayOfNulls<NdefMessage>(rawMessages.size)
                for (i in rawMessages.indices) {
                    messages[i] = rawMessages[i] as NdefMessage
                }
                // Handle NFC read data here
                val payload = String(messages[0]?.records?.get(0)?.payload ?: byteArrayOf())
                // payload will contain the NFC data read
                Toast.makeText(this, payload, Toast.LENGTH_SHORT).show()
            }
        }*/
    }


    private fun handleNfcIntent(intent : Intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action) {
            val rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            if (NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action) {
                val rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
                if (rawMessages != null) {
                    val messages = Array(rawMessages.size) { rawMessages[it] as NdefMessage }
                    GlobalScope.launch(Dispatchers.IO) {
                        val payloads = readNfcMessages(messages)
                        Toast.makeText(this@NFCTagActivity, "$payloads", Toast.LENGTH_SHORT).show()
                        withContext(Dispatchers.Main) {
                            // Handle NFC read payloads
                        }
                    }
                }
            }
        }
    }

    private fun readNfcMessages(messages: Array<NdefMessage>) : List<String>  {
        val payloads = mutableListOf<String>()
        for (message in messages) {
            for (record in message.records) {
                val payload = String(record.payload)
                payloads.add(payload)
            }
        }
        return payloads
    }

    // Function to write NFC data
    private fun writeNFCData(tag: Tag, data: String) {
        val record = NdefRecord.createTextRecord("", data)
        val message = NdefMessage(arrayOf(record))
        val ndef = Ndef.get(tag)
        ndef?.connect()
        if (ndef != null && ndef.isWritable) {
            ndef.writeNdefMessage(message)
        } else {
            val formatable = NdefFormatable.get(tag)
            formatable?.connect()
            formatable?.format(message)
        }
        ndef.close()
    }

    fun isNdefFormatted(tag: Tag): Boolean {
        val ndef = Ndef.get(tag)
        if (ndef != null) {
            try {
                ndef.connect()
                return ndef.maxSize > 0
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                ndef.close()
            }
        } else {
            val formatable = NdefFormatable.get(tag)
            if (formatable != null) {
                try {
                    formatable.connect()
                    return true
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    formatable.close()
                }
            }
        }
        return false
    }

}