package com.app.practicetest.nfc

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.nfc.tech.NfcA
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.practicetest.R
import java.io.IOException
import java.util.Locale

class NFCActivity : AppCompatActivity() {

    private var nfcAdapter : NfcAdapter? = null

    lateinit var techListArray : Array<Array<String>>
    lateinit var intentFilterArray : Array<IntentFilter>
    lateinit var pendingIntent: PendingIntent
    var mWriteMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfcactivity)

        initNFC()

    }

    private fun initNFC() {
        this.nfcAdapter = NfcAdapter.getDefaultAdapter(this)?.let { it }

        if (nfcAdapter == null) {
            showToast("NFC Hardware not available on Device")
        } else if (!nfcAdapter!!.isEnabled) {
            showToast("NFC is NOT Enabled, Please Enable NFC")
        }

        //Intent to be passed into pending intent
        val intent = Intent(this, javaClass).apply {
            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }

        //Pending Intent that brings this activity up when card is scanned
        pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        //IntentFilters Array that basically listen for whether a card is tapped
        // regardless of whether it has a string extra
        intentFilterArray = arrayOf(IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED))

        //All tags including MIFARE Classic will be detected as NfcA on all devices
        techListArray = arrayOf(arrayOf(NfcA::class.java.name))
    }

    override fun onPause() {
        super.onPause()
        if(nfcAdapter!= null)
            nfcAdapter!!.disableReaderMode(this)

        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter?.enableForegroundDispatch(this, pendingIntent, intentFilterArray, techListArray)
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (NfcAdapter.ACTION_TAG_DISCOVERED == intent!!.action) {
            var tagFromIntent: Tag? = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
            if (tagFromIntent == null)
                showToast("Empty Tag")

            var failMessage = ""

            val record: NdefRecord = NdefRecord.createTextRecord("En", "Hello, I'm Chirag.k")
            val message = NdefMessage(arrayOf(record))
            if (writeTag(message, tagFromIntent)) {
                showToast("Write...")
            } else {
                failMessage = "writingTagFailed"
                showToast("Error: Writing tag failed")
            }
            try {
                val ndef : Ndef = Ndef.get(tagFromIntent)
                if (ndef.maxSize < message.toByteArray().size) {
                    failMessage = "tagTooSmall"
                    showToast("Error: tag too small")
                } else {
                    if (ndef.canMakeReadOnly()) {
                        if (!ndef.canMakeReadOnly()) {
                            failMessage = "lockingTagFailed"
                            showToast("Error: locking tag failed")
                        }
                    } else {
                        failMessage = "tagNotLockable"
                        showToast("Error: tag not lockable")
                    }
                }
            } catch (e : Exception) {
                failMessage = "lockingTagFailed"
                Log.e("NFCValue", "System: Locking tag failed")
            }

        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.action)) {
            showToast("Empty TAg")
        }


    }

    private fun ByteArray.toHexString() : String {
        return this.joinToString("") {
            java.lang.String.format("%2x", it).uppercase(Locale.ROOT)
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    // nfc
    private fun enableTagWriteMode() {
        mWriteMode = true
        val tagDetected = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        val mWriteTagFilters = arrayOf(tagDetected)
        nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, mWriteTagFilters, null)
    }

    private fun disableTagWriteMode() {
        mWriteMode = false
        nfcAdapter!!.disableForegroundDispatch(this)
    }

    // Writes an NdefMessage to a NFC tag
    fun writeTag(message: NdefMessage, tag: Tag?): Boolean {
        val size = message.toByteArray().size
        return try {
            val ndef = Ndef.get(tag)
            if (ndef != null) {
                ndef.connect()
                if (!ndef.isWritable) {
                    showToast("Error: tag not writable")
                    return false
                }
                if (ndef.maxSize < size) {
                    showToast( "Error: tag too small")
                    return false
                }
                ndef.writeNdefMessage(message)
                true
            } else {
                val format = NdefFormatable.get(tag)
                if (format != null) {
                    try {
                        format.connect()
                        format.format(message)
                        true
                    } catch (e: IOException) {
                        false
                    }
                } else {
                    false
                }
            }
        } catch (e: Exception) {
            false
        }
    }

}