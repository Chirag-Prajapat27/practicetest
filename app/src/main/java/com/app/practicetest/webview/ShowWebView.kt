package com.app.practicetest.webview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.app.practicetest.R

class ShowWebView : AppCompatActivity() {

    var wv : WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_web_view)
        wv = findViewById(R.id.wv_tnc)
        wv?.run {

            webViewClient = WebViewClient()
            loadUrl("https://www.mygetplus.id/promo/")
            settings.javaScriptEnabled = true
            settings.setSupportZoom(false)
        }
    }

    override fun onBackPressed() {
        if (wv!!.canGoBack()) wv!!.goBack() else super.onBackPressed()
    }
}