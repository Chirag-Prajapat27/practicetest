package com.app.practicetest.pagination

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.app.practicetest.producttest.ProductPagingDataSourceHilt
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@HiltViewModel
class PagingViewModel @Inject constructor(private val pagingSource: PagingDataSourceHilt, productSource: ProductPagingDataSourceHilt) : ViewModel() {

    val quoteList = Pager(PagingConfig(2)) {
        pagingSource
    }.flow.cachedIn(viewModelScope)

    val productLIst = Pager(PagingConfig(10)) {
        productSource
    }.flow.cachedIn(viewModelScope)
        .flowOn(Dispatchers.Default)

}

