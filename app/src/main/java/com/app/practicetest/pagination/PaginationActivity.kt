package com.app.practicetest.pagination

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import com.app.practicetest.R
import com.app.practicetest.databinding.ActivityPaginationBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PaginationActivity : AppCompatActivity() {

    private val pagingViewModel: PagingViewModel by viewModels()

    @Inject
    lateinit var adapter: QuoteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityPaginationBinding = DataBindingUtil.setContentView(this, R.layout.activity_pagination)

        binding.rvQuotes.adapter = adapter

        lifecycleScope.launchWhenStarted {
            adapter.loadStateFlow.collect {
                binding.prependProgress.isVisible = it.source.prepend is LoadState.Loading
                binding.appendProgress.isVisible = it.source.append is LoadState.Loading
            }
        }

        lifecycleScope.launchWhenStarted {
                pagingViewModel.quoteList.collectLatest {
                    adapter.submitData(it)
                }
        }


//        lifecycleScope.launchWhenStarted {
//            pagingViewModel.quoteList.collect {
//                Log.d("Data", "Hilt Data paging $it")
//                adapter.submitData(it)
//            }
//        }


    }
}