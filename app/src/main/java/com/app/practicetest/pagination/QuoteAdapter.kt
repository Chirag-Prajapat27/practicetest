package com.app.practicetest.pagination

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.practicetest.databinding.ItemQuotesBinding
import javax.inject.Inject

class QuoteAdapter @Inject constructor() : PagingDataAdapter<Result, QuoteAdapter.MyViewHolder>(DiffObj) {

    class MyViewHolder(binding: ItemQuotesBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.quotesList = getItem(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemQuotesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    object DiffObj : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
    }

}
