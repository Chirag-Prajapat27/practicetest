package com.app.practicetest.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.practicetest.api.ApiServices
import kotlinx.coroutines.delay
import javax.inject.Inject

private const val LOAD_DELAY_MILLIS = 3_000L

class PagingDataSourceHilt @Inject constructor(private val apiServices: ApiServices): PagingSource<Int, Result>() {

    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {

        return state.anchorPosition?.let { anchorPosition->

            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {

        val currentKey =params.key?: 103

        val response = apiServices.getAllQuotes(currentKey)

        val responseData = mutableListOf<Result>()
        val data = response.results
        responseData.addAll(data)

        val prevKey = if(currentKey==1) null else currentKey-1

        if (currentKey  != 1) delay(LOAD_DELAY_MILLIS)
        return LoadResult.Page(data,prevKey,currentKey.plus(1))

    }
}