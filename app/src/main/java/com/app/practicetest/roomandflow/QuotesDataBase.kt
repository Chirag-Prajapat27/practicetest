package com.app.practicetest.roomandflow

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Database(entities = [Quotes::class], version = 1, exportSchema = false)
abstract class QuotesDataBase : RoomDatabase() {

    abstract fun daoQuotes(): QuotesDao

    @Module
    @InstallIn(SingletonComponent::class)
    object RoomAppModule {

        @Singleton
        @Provides
        fun quotesDataBase(@ApplicationContext context: Context) = Room.databaseBuilder(
            context, QuotesDataBase::class.java, "quotes_database").build()

        @Singleton
        @Provides
        fun providerDao(dataBase: QuotesDataBase) = dataBase.daoQuotes()
    }

}