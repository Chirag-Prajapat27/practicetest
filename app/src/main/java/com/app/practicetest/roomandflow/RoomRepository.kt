package com.app.practicetest.roomandflow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RoomRepository @Inject constructor(private val dao: QuotesDao) {

    fun getAllQuote() : Flow<List<Quotes>> = dao.getAllQuote()

    suspend fun insertQuotes(quotes: Quotes) = dao.insertQuoteData(quotes)

    suspend fun deleteQuotes(quotes: Quotes) = dao.deleteQuotes(quotes)

    suspend fun clearQuotes() = dao.clearQuotes()

    fun getAllNormalQuotes() = dao.getAllNormalQuotes()

    fun getAllLiveQuotes() = dao.getAllLiveQuotes()

}