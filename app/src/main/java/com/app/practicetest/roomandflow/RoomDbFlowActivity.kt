package com.app.practicetest.roomandflow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.app.practicetest.R
import com.app.practicetest.databinding.ActivityRoomDbFlowBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RoomDbFlowActivity : AppCompatActivity() {

    private val viewModel : QuotesRoomViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_room_db_flow)

        val binding : ActivityRoomDbFlowBinding = DataBindingUtil.setContentView(this,R.layout.activity_room_db_flow)

        insertQuoteData()
        binding.btnReadData.setOnClickListener {
            getData(binding)
        }

    }

    //create (Insert)
    private fun insertQuoteData() {

        val quotes = Quotes("1","chirag","chirag","courage is looking fear right in the eye and saying, get the hell out of way, I've got things to do",
            "08-07-2022", "08-07-2022",10)

        CoroutineScope(Dispatchers.Main).launch {
            viewModel.insertQuotes(quotes)
        }
    }

    //READ
    private fun getData(binding: ActivityRoomDbFlowBinding) {
                val adapter = QuotesRoomAdapter()

        lifecycleScope.launch {
            viewModel.getAllQuote().collect {
                binding.rvRoomDataQuotes.adapter = adapter
                adapter.submitList(it)
            }
        }
    }

}