package com.app.practicetest.roomandflow

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class QuotesRoomViewModel @Inject constructor(private val repository: RoomRepository) : ViewModel() {

    fun getAllQuote() = repository.getAllQuote()

    suspend fun insertQuotes(quotes: Quotes) = repository.insertQuotes(quotes)

    suspend fun deleteQuotes(quotes: Quotes) = repository.deleteQuotes(quotes)

    suspend fun clearQuotes() = repository.clearQuotes()

    fun getAllNormalQuotes() = repository.getAllNormalQuotes()

    fun getAllLiveQuotes() = repository.getAllLiveQuotes()
}