package com.app.practicetest.roomandflow

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface QuotesDao {

    @Query("Select * from quote_table")
    fun getAllQuote(): Flow<List<Quotes>>

    @Insert
    suspend fun insertQuoteData(quotes: Quotes)

    @Delete
    suspend fun deleteQuotes(quotes: Quotes)

    @Query("DELETE FROM quote_table")
    suspend fun clearQuotes()

    @Query("SELECT * FROM quote_table ")
    fun getAllNormalQuotes(): LiveData<List<Quotes>>

    @Query("SELECT * FROM quote_table ORDER BY _id DESC")
    fun getAllLiveQuotes(): LiveData<List<Quotes>>
    // why not use suspend ? because Room does not support LiveData with suspended functions.
    // LiveData already works on a background thread and should be used directly without using coroutines

}