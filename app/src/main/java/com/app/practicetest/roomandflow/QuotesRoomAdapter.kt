package com.app.practicetest.roomandflow

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.practicetest.databinding.ItemQuotesRoomBinding
import com.app.practicetest.pagination.QuoteAdapter

class QuotesRoomAdapter : ListAdapter<Quotes,QuotesRoomAdapter.MyViewHolder>(DiffUtil()) {

    class MyViewHolder(binding: ItemQuotesRoomBinding) : RecyclerView.ViewHolder(binding.root) {
        val myBinding = binding
    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<Quotes>() {
        override fun areItemsTheSame(oldItem: Quotes, newItem: Quotes): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: Quotes, newItem: Quotes): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemQuotesRoomBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.myBinding.quotesList = getItem(position)
    }

}