package com.app.practicetest.servicess.boundService

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import java.util.Random

class MyBoundService : Service() {

    // Binder given to client
    private val binder = LocalBinder()

    // Random number generator
    private val mGenerator = Random()

    /**
     * Method for clients
     */
    val randomNumber : Int
        get() = mGenerator.nextInt(10)

    /**
     * Class used for the client Binder. bcz we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of MyBoundService sp client can call public methods.
        fun getService() : MyBoundService = this@MyBoundService
    }

    override fun onBind(intent: Intent): IBinder {
       return binder
    }
}