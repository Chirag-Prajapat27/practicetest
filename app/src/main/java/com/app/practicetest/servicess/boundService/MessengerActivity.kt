package com.app.practicetest.servicess.boundService

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import com.app.practicetest.databinding.ActivityMessengerBinding

class MessengerActivity : AppCompatActivity() {

    lateinit var binding: ActivityMessengerBinding

    /** Messenger for communicating with the service.  */
    private var service: Messenger? = null

    /** Flag indication whether we have called bind on the service */
    private var bound: Boolean = false

    /** Class for interacting of the main interface of the service */
    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            // This is called when the connection with the service has been established,
            // giving us the object we can use to interact with the service,
            // We are communicating with the service using a Messenger,
            // so here we gwt a client-side representation of
            // that from the raw IBinder object.
            service = Messenger(p1)
            bound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            // this is called when thw connection with the service has been
            // unexpectedly disconnection&mdash; that is, its process crashed.
            service = null
            bound = false
        }
    }

    private fun sayHello() {
        if (!bound) return
        // Create & send message to the service, using a supported 'what' value.
        val msg: Message = Message.obtain(null, MSG_SAY_HELLO, 0, 0)
        try {
            service?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessengerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCallService.setOnClickListener {
            sayHello()
        }
    }

    override fun onStart() {
        super.onStart()
        //Bind to service
        Intent(this, MessengerService::class.java).also {
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        // Unbind to service
        if (bound) {
            unbindService(mConnection)
            bound = false
        }
    }

}