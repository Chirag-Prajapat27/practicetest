package com.app.practicetest.servicess.boundService

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.practicetest.databinding.ActivityBindingBinding

class BindingActivity : AppCompatActivity() {

    private lateinit var bindService : MyBoundService
    private var mBound : Boolean = false
    lateinit var binding : ActivityBindingBinding

    /** Define callbacks for services binding, passed to bindService().*/
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            // we've bound to MyBoundService, cast the IBinder and get MyBoundService instance.
            val binder = service as MyBoundService.LocalBinder
            bindService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            mBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBindingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRandomCount.setOnClickListener {
            if (mBound) {
                val num: Int = bindService.randomNumber
                Toast.makeText(this, "number $num", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        //Bind to MyBoundService
        Intent(this, MyBoundService::class.java).also {
            bindService(it, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        mBound = false
    }

}