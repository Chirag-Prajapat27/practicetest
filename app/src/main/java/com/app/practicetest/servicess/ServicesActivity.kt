package com.app.practicetest.servicess

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import com.app.practicetest.R
import com.app.practicetest.databinding.ActivityServicesBinding


class ServicesActivity : AppCompatActivity() {

    lateinit var binding : ActivityServicesBinding

    lateinit var service: ForegroundService
    var isBound : Boolean = false

    private val connection  = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binding = p1 as ForegroundService.MyIBinders

            service = binding.getService()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_services)
       binding = ActivityServicesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        runService()

        //call normal service
//        startService(Intent(this, SampleService::class.java))

        binding.btnStartForeService.setOnClickListener {
            //call Foreground Service
            val intent = Intent(this, ForegroundService::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                applicationContext.startForegroundService(intent)
            }
        }

        binding.btnStopForeService.setOnClickListener {
            val intent = Intent(this, ForegroundService::class.java)
            applicationContext.stopService(intent)
            unbindService(connection)
        }

        binding.ivPlayPause.setOnClickListener {
            if (service.isPlay()) {
                service.musicPause()
                binding.ivPlayPause.setImageDrawable(getDrawable(R.drawable.ic_play_arrow))
            } else {
                service.musicPlay()
                binding.ivPlayPause.setImageDrawable(getDrawable(R.drawable.ic_pause))
            }
        }
    }

    private fun runService() {
        val intent = Intent(this, ForegroundService::class.java)
        bindService(intent,connection, BIND_AUTO_CREATE)
    }

    private fun stopService() {
        val intent = Intent(this, ForegroundService::class.java)
        unbindService(connection)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}