package com.app.practicetest.servicess

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.app.practicetest.R
import com.app.practicetest.musicservice.MusicServiceActivity
import com.app.practicetest.utils.CHANNEL_ID

class ForegroundService : Service() {

    private lateinit var mediaPlayer: MediaPlayer
    val binder = MyIBinders()

    inner class MyIBinders : Binder() {
        fun getService() : ForegroundService {
            return this@ForegroundService
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)
        mediaPlayer.isLooping = false
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "Start Foreground Service", Toast.LENGTH_SHORT).show()

        createNotificationChannel()

        // If the notification supports a direct reply action, use
        // PendingIntent.FLAG_MUTABLE instead.
        val pendingIntent: PendingIntent =
            Intent(this, MusicServiceActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_IMMUTABLE )
            }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(if (this.isPlay()) "Play" else "Pause")
            .setContentText(getText(R.string.str_notification_message))
            .setSmallIcon(if (this.isPlay()) R.drawable.ic_pause else R.drawable.ic_play_arrow)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setTicker(getText(R.string.str_ticket_text))
            .setAutoCancel(true)
            .build()

        startForeground(102, notification)

        return START_STICKY
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val name = getText(R.string.not_channel_name)
            val descriptionText = getString(R.string.notification_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "Stop Foreground Service", Toast.LENGTH_SHORT).show()
    }

    fun musicPlay() {
        mediaPlayer.start()
    }

    fun musicPause() {
        mediaPlayer.pause()
    }

    fun isPlay(): Boolean {
        return mediaPlayer.isPlaying
    }

    fun musicStop() {
        mediaPlayer.stop()
    }

}