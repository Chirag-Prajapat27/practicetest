package com.app.practicetest.test

import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback
import androidx.lifecycle.*
import com.app.practicetest.api.Resource
import com.app.practicetest.pagination.QuotesModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TestViewModel @Inject constructor(private val repository: TestRepository) : ViewModel() {


//    private val _weatherForecast = repository.fetchWeatherForecast().asLiveData()

//        private val _weatherForecast1 = MutableLiveData<Resource<QuotesModel>>()

        private val _weatherForecast1 = MutableLiveData<Resource<QuotesModel>>(Resource.Loading())

//    fun getAllData() = liveData(Dispatchers.IO) {
//        repository.fetchWeatherForecast().toL
//    }


   //========================= Flow ========================

//    private val _weatherForecast = repository.fetchWeatherForecast()
//
//        .onStart { emit(Resource.Loading()) }
//        .filter {
//            delay(1000)
//            if (it is Resource.Success) {
//                it.data!!.page < 10
//            } else true
//            //Do nothing if result is loading or error
//        }
//        .buffer()
//        .onEach {
//            Log.d("Test", "$it it is modified & reached until onEach operator") }
//        .flowOn(Dispatchers.Default)
//        .catch { throwable ->
//            Log.d("Test", throwable.toString()) }
//
//
//    val weatherForecast: Flow<Resource<QuotesModel>>
//        get() = _weatherForecast

    //========================================================================================

    //Using with liveData and coroutine

//    private val _weatherForecast = MutableLiveData<Resource<QuotesModel>>()

//    init {
//        fetchWeatherForecast()
//    }
//
//    private fun fetchWeatherForecast() {
//        _weatherForecast.value = Resource.Loading()
//
//        viewModelScope.launch {
//            _weatherForecast.value = repository.fetchWeatherForecastNormal()
//
//        }
//    }
//        val weatherForecast:  MutableLiveData<Resource<QuotesModel>>
//        get() = _weatherForecast

}