package com.app.practicetest.test

import com.app.practicetest.api.ApiServices
import com.app.practicetest.api.Resource
import com.app.practicetest.pagination.QuotesModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TestRepository @Inject constructor(private val services: ApiServices){

    fun fetchWeatherForecast() : Flow<Resource<QuotesModel>> = flow {

        emit(Resource.Loading())

            val response = services.getAllData()
        emit(Resource.Success(response,"mSg"))

    }

    //for Live Data and coroutine
//    suspend fun fetchWeatherForecastNormal() : Resource<QuotesModel> {
//        //Fake api call
////        delay(1000)
//        val response = services.getAllData()
//        //send a random fake weather forecast data
//        return Resource.Success(response,"mSg")
//    }


    //for fetchData from local DB
//    suspend fun fetchWeatherForecast(shouldGetFromApi: Boolean) {
//        if (shouldGetFromApi) {
//            //Api Call
//        } else {
//            //Get it from catch or local db
//        }
//    }

}