package com.app.practicetest.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.app.practicetest.R
import com.app.practicetest.api.Resource
import com.app.practicetest.custome.CustomDialog
import com.app.practicetest.databinding.ActivityTestBinding
import com.app.practicetest.utils.errorSnack
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TestActivity : AppCompatActivity() {

    private val viewModel : TestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityTestBinding = DataBindingUtil.setContentView(this,R.layout.activity_test)


//        lifecycleScope.launchWhenStarted {
//            viewModel.weatherForecast.collect { response ->
//                when(response) {
//                    is Resource.Loading -> {
//                        CustomDialog.getInstance().showDialog(this@TestActivity)
//                    }
//                    is Resource.Success -> {
//                        CustomDialog.getInstance().hide()
//                        response.data!!.let {
//                            //Update weather Data
//                            binding.tvTestText.text = it.toString()
//                        }
//                    }
//                    is Resource.Error -> {
//                        CustomDialog.getInstance().hide()
//                        binding.rootLayout.errorSnack(response.message!!, Snackbar.LENGTH_LONG)
//                    }
//                }
//            }
//        }


        //for using Live data and coroutine
//        viewModel.weatherForecast.observe(this, Observer {
//            when(it) {
//                is Resource.Loading -> {
//                    Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show()
//                }
//                is Resource.Success -> {
//                    //Update weather Data
//                    binding.tvTestText.text = it.data.toString()
//                }
//                is Resource.Error -> {
//                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
//                }
//            }
//        })

    }
}