package com.app.practicetest.test

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.app.practicetest.R

class BrowserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        val btn = findViewById<Button>(R.id.btn_openBrowser)

        btn.setOnClickListener {
            val url = "https://www.mygetplus.id/blog/kata-gita/cara-dapat-saldo-dana-getplus/?browser=true"
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url) ))
        }
    }
}