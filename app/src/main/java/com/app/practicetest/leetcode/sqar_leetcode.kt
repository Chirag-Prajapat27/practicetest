package com.app.practicetest.test

fun main() {

    print(mySqrt(11))
}

fun mySqrt(x: Int): Int {
    var st : Long = 0
    var end  = x.toLong()

    while (st + 1 < end) {
        val mid = st + (end - st)/2

        if (mid * mid == x.toLong()) {
            return mid.toInt()
        } else if (mid * mid < x) {
            st = mid
        } else {
            end = mid
        }
    }
    if (end * end == x.toLong()) {
        return end.toInt()
    }
    return st.toInt()
}