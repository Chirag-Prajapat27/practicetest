package com.app.practicetest.leetcode

fun main() {
    val grid1 : Array<IntArray> = arrayOf(
        intArrayOf(4, 3, 2, -1),
        intArrayOf(3, 2, 1, -1),
        intArrayOf(1, 1, -1, -2),
        intArrayOf(-1, -1, -2, -3))

    val grid : Array<IntArray> = arrayOf(intArrayOf(3,2), intArrayOf(1,0))

    print(countNegatives(grid1))
    print("\n"+countNegatives(grid))
}

fun countNegativess(grid: Array<IntArray>): Int {
    var res = 0
    for (i in grid.indices) {
        for (j in grid[0].indices.reversed()) {
            if (grid[i][j] < 0) res++
        }
    }
    return res
}

fun countNegatives(grid: Array<IntArray>): Int {
    var res = 0
    for (i in grid.indices) {
        for (j in grid[0].indices) {
            if (grid[i][j] < 0) res++
        }
    }
    return res
}

