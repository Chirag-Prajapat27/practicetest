package com.app.practicetest.test

fun main() {
   val num = intArrayOf(0, 4, 3, 4, 0)
   print(specialArray(num).toString())
}

fun specialArray(nums: IntArray) : Int {
    val n = nums.size
    for (i in 0..n) {
        var c = 0
        for (j in nums) {
            if(j>=i) c++
        }
        if(i == c) return i
    }
    return -1
}