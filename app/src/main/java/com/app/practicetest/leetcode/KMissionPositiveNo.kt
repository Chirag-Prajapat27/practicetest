package com.app.practicetest.leetcode

fun main() {
    print(findKthPositive(intArrayOf(2,3,4,7,11), 5))
    print(findKthPositive(intArrayOf(6,7,8), 5))
}
fun findKthPositive(arr: IntArray, k: Int): Int {
    val n = arr.size
    var left = 0
    var right = n - 1
    var missing = 0

    while (left <= right) {  ///04, 34, 44, f  ///02,
        val mid = left + (right - left) / 2  ///2, 3, 4, ///1,0

        missing = compute(arr[mid], mid + 1) ///1, 3, 6,  ///5, 5

        if (missing >= k) ///f, f, t  ///t, t
            right = mid - 1  ///4,4,3  ///0, -1
        else
            left = mid + 1  ///3,4,4
    }

    // Right -> -1
    return if (right == -1)
        k
    else
        arr[right] + k - compute(arr[right], right + 1)  ///7+5 - 7-4 = 11-3 = 9
}

fun compute(actual: Int, expected: Int): Int {
    return actual - expected
}