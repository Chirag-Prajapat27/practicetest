package com.app.practicetest.leetcode

fun main() {
    print ("Big O(n):: "+arrangeCoinsOn(5))
    print ("\nBig O(log n):: "+arrangeCoinsOLogn(1804289383))
    print ("\nBig O(1):: "+arrangeCoinsO1(1804289383))
}

///Bing O(n) Time
fun arrangeCoinsOn(n: Int): Int {
    var i = 1
    var res = 0
    var n1 = n
         while (n1 >= i) {
             n1 -= i
             res ++
             i++
         }
         return res
}

///Bing O(log n) Time
fun arrangeCoinsOLogn(n: Int): Int {
    var left : Long = 0
    var right = n

    while (left.toInt() <= right) {
        val mid = left.toInt() + (right - left.toInt())/2

        if (n >= (mid * (mid+1)/2)) {
            left = (mid + 1).toLong()
        } else {
            right = mid -1
        }
    }
    return left.toInt() -1
}

///Bing O(1) Time
 fun arrangeCoinsO1(n: Int): Int {
    return ((Math.sqrt(2 * n.toLong() + 0.25) - 0.5).toInt())
}
