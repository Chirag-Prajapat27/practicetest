package com.app.practicetest.api

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class RetrofitBuilder {

    @Provides
    fun baseUrlProvider() : String = "https://api.quotable.io/"

//    @Provides
//    fun baseUrlProvider() : String = "http://vlpl.lrdevteam.com/api/"

    private val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    fun okHttpClientBuilder() : OkHttpClient.Builder = OkHttpClient.Builder().addInterceptor(logger)
        .addInterceptor(HeaderInterceptor())

    @Provides
    fun retrofitBuilder( baseUrl: String, okHttpClient: OkHttpClient.Builder) : Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient.build()).build()

    @Provides
    fun quotesApi(retrofit: Retrofit) : ApiServices = retrofit.create(ApiServices::class.java)


//    @Provides
//    fun <T> buildService(serviceType: Class<T>,retrofit: Retrofit): T = retrofit.create(serviceType)


}