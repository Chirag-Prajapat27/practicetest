package com.app.practicetest.api

import com.app.practicetest.pagination.Product
import com.app.practicetest.pagination.QuotesModel
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiServices {

    @GET("quotes")
    suspend fun getAllQuotes(@Query("page") page: Int): QuotesModel

    @POST("product/getallproducts")
    suspend fun getAllProduct(@Query("page") page: Int): Product

    @GET("quotes")
    suspend fun getAllData() : QuotesModel

}