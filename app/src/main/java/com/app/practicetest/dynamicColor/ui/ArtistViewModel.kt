//package com.app.practicetest.dynamicColor.ui
//
//import androidx.lifecycle.LiveData
//import androidx.lifecycle.MutableLiveData
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.viewModelScope
//import com.app.practicetest.dynamicColor.ArtistRepo
//import com.app.practicetest.dynamicColor.api.Resorces
//import com.app.practicetest.dynamicColor.model.Result
//import dagger.hilt.android.lifecycle.HiltViewModel
//import kotlinx.coroutines.launch
//import javax.inject.Inject
//
//@HiltViewModel
//class ArtistViewModel @Inject constructor(private val repository : ArtistRepo) : ViewModel() {
//
//    private val _list = MutableLiveData<Resorces<Result>>()
//
//    val list : LiveData<Resorces<Result>>
//        get() = _list
//
//    init {
//        getList()
//    }
//
//    private fun getList()  = viewModelScope.launch {
//        _list.postValue(Resorces.loading(null))
//        repository.getList().let {
//            if (it.isSuccessful) {
//                _list.postValue(Resorces.success(it.body()))
//            } else {
//                _list.postValue(Resorces.error(it.errorBody().toString(), null))
//            }
//        }
//
//    }
//
//}