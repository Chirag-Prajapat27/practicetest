package com.app.practicetest.dynamicColor.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.app.practicetest.R
import com.app.practicetest.databinding.FragmentArtistBinding
import com.app.practicetest.dynamicColor.ViewPagerAdapter
import com.app.practicetest.dynamicColor.api.Status
import com.app.practicetest.dynamicColor.model.ResultItem
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArtistFragment : Fragment(R.layout.fragment_artist) {


    private lateinit var binding : FragmentArtistBinding
    private lateinit var mAdapter : ViewPagerAdapter
//    private val viewModel : ArtistViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_artist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentArtistBinding.bind(view)

        binding.viewPager2.clipChildren = false
        binding.viewPager2.setPadding(0,0,0,0)
        mAdapter = ViewPagerAdapter()
        binding.viewPager2.adapter = mAdapter

        mAdapter.submitList(getList())

       /* viewModel.list.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data.let { result ->
                        if (result != null) {
                            mAdapter.submitList(result)
                        } else {
                            Snackbar.make(view, "Status = false", Snackbar.LENGTH_SHORT).show()
                        }
                    }
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Snackbar.make(view, "Something went wrong", Snackbar.LENGTH_SHORT).show()

                }
                Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
            }
        })*/

    }

    fun getList() = listOf(
        ResultItem("India Map", "https://as2.ftcdn.net/v2/jpg/03/42/07/49/1000_F_342074919_vEg2dERX7y5h4f59yiCqRx5dl6AyBcEx.jpg"),
        ResultItem("India's Flag", "https://as2.ftcdn.net/v2/jpg/00/79/07/81/1000_F_79078102_2fa1JMfQY2nsWrYpsegWvHxPWHjwBZl4.jpg"),
        ResultItem("India's Flag in Sky", "https://as2.ftcdn.net/v2/jpg/03/57/53/09/1000_F_357530978_Y6if1rFTPjWJ24DQdnx7YVZiZIkCao7z.jpg"),
        ResultItem("Billie Eilish", "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/source/biliie.png"),
        ResultItem("Hadise", "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/source/hadise.png"),
        ResultItem("Najwa Farouk", "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/source/najwa.png"),
        ResultItem("Selena Gomez", "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/source/selena.png"),
        ResultItem("Tamino", "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/source/tamino.png"),
        ResultItem("Anuska Sharma", "https://www.mrdustbin.com/wp-content/uploads/2020/03/%E0%A4%85%E0%A4%A8%E0%A5%81%E0%A4%B7%E0%A5%8D%E0%A4%95%E0%A4%BE-%E0%A4%B6%E0%A4%B0%E0%A5%8D%E0%A4%AE%E0%A4%BE-819x1024.jpg"),
        ResultItem("Jacqueline Fernandez", "https://www.mrdustbin.com/wp-content/uploads/2020/03/%E0%A4%9C%E0%A5%88%E0%A4%95%E0%A4%B2%E0%A5%80%E0%A4%A8-%E0%A4%AB%E0%A4%B0%E0%A5%8D%E0%A4%A8%E0%A4%BE%E0%A4%82%E0%A4%A1%E0%A5%80%E0%A4%9C-1004x1024.jpg"),
        ResultItem("Deepika Padukone", "https://www.mrdustbin.com/en/wp-content/uploads/2021/08/deepika-padukone.jpg"),
        ResultItem("Urvashi Rautela", "https://www.mrdustbin.com/wp-content/uploads/2020/04/%E0%A4%89%E0%A4%B0%E0%A5%8D%E0%A4%B5%E0%A4%B6%E0%A5%80-%E0%A4%B0%E0%A5%8C%E0%A4%A4%E0%A5%87%E0%A4%B2%E0%A4%BE-Urvashi-Rautela-820x1024.jpg"),
        ResultItem("Lena Kumar", "https://sp-ao.shortpixel.ai/client/to_webp,q_glossy,ret_img,w_768/https://www.mrdustbin.com/wp-content/uploads/2020/05/Lena-kumar-768x860.jpg"),
        ResultItem("Shraddha Kapoor", "https://www.mrdustbin.com/wp-content/uploads/2020/03/%E0%A4%B6%E0%A5%8D%E0%A4%B0%E0%A4%A6%E0%A5%8D%E0%A4%A7%E0%A4%BE-%E0%A4%95%E0%A4%AA%E0%A5%82%E0%A4%B0-819x1024.jpg"),
        ResultItem("Sonam Kapoor", "https://www.mrdustbin.com/wp-content/uploads/2020/03/%E0%A4%B8%E0%A5%8B%E0%A4%A8%E0%A4%AE-%E0%A4%95%E0%A4%AA%E0%A5%82%E0%A4%B0-819x1024.jpg"),
        ResultItem("Vin Diesel", "https://www.mrdustbin.com/en/wp-content/uploads/2021/05/vin-diesel-768x790.jpg"),
    )

}