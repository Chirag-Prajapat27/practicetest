//package com.app.practicetest.dynamicColor.api
//
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.components.SingletonComponent
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import javax.inject.Singleton
//
//@Module
//@InstallIn(SingletonComponent::class)
//object AppModule {
//
//    @Provides
//    fun provideURL() = "https://raw.githubusercontent.com/betulnecanli/DominantColorBackground/master/"
//
//    @Provides
//    @Singleton
//    fun providerRetrofit(url : String) : ResultApi =
//        Retrofit.Builder()
//            .baseUrl(url)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//            .create(ResultApi::class.java)
//
//}