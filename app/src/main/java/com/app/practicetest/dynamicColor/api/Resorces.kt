package com.app.practicetest.dynamicColor.api

data class Resorces<out T>(
    val status : Status,
    val data : T?,
    val message: String?
) {
    companion object {
        fun <T> success(data:T?) : Resorces<T> {
            return Resorces(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data:T?) : Resorces<T> {
            return Resorces(Status.ERROR, data, msg)
        }

        fun <T> loading(data:T?) : Resorces<T> {
            return Resorces(Status.LOADING, data, null)
        }

    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
