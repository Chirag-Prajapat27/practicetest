package com.app.practicetest.dynamicColor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.practicetest.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DynamicColorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_color)


    }

}
