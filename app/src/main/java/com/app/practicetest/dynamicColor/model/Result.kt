package com.app.practicetest.dynamicColor.model

import com.google.gson.annotations.SerializedName

class Result : ArrayList<ResultItem>()

data class ResultItem(
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String
)